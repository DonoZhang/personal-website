import React, {Component} from 'react'
import PropTypes from 'prop-types'

//dumb component

export default class Page extends Component{
    static propTypes = {
        children: PropTypes.array,
        className: PropTypes.string
    }
    render(){
        return (
            <div className={`page ${this.props.className}`}>
                <div className="page-header">{this.props.children[0]}</div>
                <div className="page-content">{this.props.children[1]}</div>
            </div>
        )
    }
}