import React, {Component} from 'react'
import PropTypes from 'prop-types'
import responsiveWrapper from '../advancedComponents/responsiveWrapper'
import Page from './Page'
import profile from '../../Profile.JPG'

//smart component - contains smart component responsiveWrapper

class Home extends Component{
	static propTypes={
		trigger: PropTypes.bool
	}

	render(){
		return (
				<Page className='home'>
					<div>
						<img className='profile' src={profile} alt='profile'
						style={this.props.trigger?{display: 'block', margin: 'auto'}:
						{display: 'inline-block'}}/>
						<div style={this.props.trigger?{display: 'block', margin: 'auto'}:
						{display: 'inline-block'}}>
							<p>Dapeng Zhang</p>
							<p>Front End Web Developer(React)</p>
						</div>
					</div>
					{'Content'}
				</Page>
		)
	}
}


export default responsiveWrapper(Home, 'md', -1)