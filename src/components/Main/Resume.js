import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Page from './Page'

export default class Resume extends Component{
    render(){
        return (
            <Page>
                {'Resume'}
                {'Content'}
            </Page>
        )
    }
}