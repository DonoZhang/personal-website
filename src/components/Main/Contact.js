import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Page from './Page'

export default class Contact extends Component{
    render(){
        return (
            <Page>
                {'Contact'}
                {'Content'}
            </Page>
        )
    }
}