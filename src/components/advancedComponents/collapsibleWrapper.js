import React, {Component} from 'react'
import PropTypes from 'prop-types'

//dumb component

//make a component collapsible by clicking a button
//can be disabled when 'disabled' property is true

export default (WrappedClass)=>{
    class CollapsibleWrapper extends Component{
        static propTypes={
            enabled: PropTypes.bool
        }

        static defaultProps={
            enabled: true
        }
        
        constructor(){
            super()
            this.state={
                height: '0px'
            }
        }
    
        onCollapse = ()=>{
            const node = this.div
            let height = 'auto'
            if(node.style.height === '0px')
                height = 'auto'
            else
                height = '0px'
            this.setState({height})
        }

        render(){
            return (
                <div>
                    <button onClick={this.onCollapse} className="collapse-button" style={
                        {display: this.props.enabled?'block':'none'}
                    }></button>
                    <div ref={(div)=>this.div = div} style={{overflow: 'hidden', height: this.props.enabled?this.state.height:'auto'}}>
                        <WrappedClass {...this.props}/>
                    </div>
                </div>
            )
        }
    }
    return CollapsibleWrapper
}