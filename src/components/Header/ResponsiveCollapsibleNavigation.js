import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Navigation from './Navigation'
import collapsibleWrapper from '../advancedComponents/collapsibleWrapper'
import responsiveWrapper from '../advancedComponents/responsiveWrapper'

//smart component - contains smart component responsiveWrapper

//Wrap Navigation with collapsibleWrapper and responsiveWrapper so that it will be collapsible at certain window width

//Firstly, add a feature that user can enable collapsibility by setting 'trigger'(bool); 
//and Navigation's 'display' property will change accordingly
const Collapse = collapsibleWrapper(Navigation)
class CollapsibleNavigation extends Component{
  static propTypes = {
    trigger: PropTypes.bool
  }
	render(){
		return (
			<div className='collapsible-navigation' style={{display: 'inline-block'}}>
					<Collapse enabled={this.props.trigger} display={this.props.trigger?'block':'inline-block'} {...this.props}/>
			</div>);
	}
}

//Secondly, add the viewport-width-responsive feature:
//collapsible when viewport-width is smaller (-1) than 992px ('md') in this case
export default responsiveWrapper(CollapsibleNavigation, 'md', -1)
