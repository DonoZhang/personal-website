import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Navigation from './ResponsiveCollapsibleNavigation'

//smart component - contains smart component Navigation

export default class Header extends Component{
    static propTypes = {
        brand: PropTypes.string
    }

    render(){
        return (
            <div className="header-wrapper" 
            style={{position: 'fixed',top: '0', width: '100vw'}}>
                <div className="header" style={
                        {textAlign: 'right'}
                }>
                    <h1 className="header-brand" style={{position: 'fixed'}}>{this.props.brand}</h1>
                    <Navigation {...this.props}/>
                </div>
            </div>
        );
    }
}