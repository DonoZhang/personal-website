import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom' 

//dumb component

//a simple navigation bar with its items' display style(block, inline...) set with 'display' property
//need to be wrapped within a <Router></Router> with according <Route/> specified if page.navType === 'LINK' (use router)

export default class Navigation extends Component{
	static propTypes = {
		pagesData: PropTypes.array,
		display: PropTypes.string
	}

	static defaultProps = {
		pagesData: []
	}

	_linksCreator(pagesData){
		let links = []
		pagesData.forEach((page, index)=>{
			if(page.navType === 'LINK')
				links.push(
					<Link key={index} to={page.path} className='navigation-item' style={{display: this.props.display}}>{page.name}</Link>
				)
		})
		return links;
	}

	// _anchorsCreator(pagesData){
	// 	let anchors = []
	// 	pagesData.forEach((page, index)=>{
	// 		if(page.navType === 'ANCHOR')
	// 			anchors.push(
	// 				<a key={index} href={page.path} className='navigation-item' style={{display: this.props.display}}>{page.name}</a>
	// 			)
	// 	})
	// 	return anchors;
	// }

	render(){
		return (
			<nav className="navigation">
				{this._linksCreator(this.props.pagesData)}
			</nav>);
	}
}
