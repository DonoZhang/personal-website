import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Header from '../components/Header/Header'
import Home from '../components/Main/Home'
import Resume from '../components/Main/Resume'
import Contact from '../components/Main/Contact'

//dispatch viewportWidth (so that only one window-resize listener is required for the whole website)
//connect navigation and pages by wrapping them with Router

//data to initialize navigation; Router is used in this case
const pagesData=[
	{navType: 'LINK', path: '/home', name: 'Home', component: Home},
	{navType: 'LINK', path: '/resume', name: 'Resume', component: Resume},
	{navType: 'LINK', path: '/contact', name: 'Contact', component: Contact}
]

class PersonalWebsite extends Component{
	static propTypes = {
		onResize: PropTypes.func
	}

	componentDidMount(){
		window.addEventListener('resize', this._handleResize)
	}

	componentWillUnmount(){
		window.removeEventListener(this._handleResize)
	}

	_handleResize = ()=>{
		if(this.props.onResize){
			this.props.onResize(window.innerWidth)
		}
	}

	_createRoutes(pagesData){
		const routes = []
		pagesData.forEach((page, index)=>routes.push(<Route key={index} path={page.path} exact component={page.component}/>))
		return routes
	}

	render(){
		return (<Router>
							<Header brand='Dapeng Zhang' pagesData={pagesData}/>{/*pass 'pagesData' down to 'Navigation'*/}
							{this._createRoutes(pagesData)}
						</Router>);
	}
}


const mapDispatchToProps = (dispatch)=>{
	return {
		onResize: (viewportWidth)=>{
			dispatch({type: 'UPDATE_VIEWPORT_WIDTH', viewportWidth})
		}
	}
}

export default connect(null, mapDispatchToProps)(PersonalWebsite)