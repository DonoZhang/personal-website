const initState = {
    viewportWidth: window.innerWidth
}

//to be dispatched by 'PersonalWebsite'
//to be connected by 'responsiveWrapper' 
export default function(state = initState, action){
    switch(action.type){
        case 'UPDATE_VIEWPORT_WIDTH': 
            return {...state, viewportWidth: action.viewportWidth}
        default:
            return state
    }
}